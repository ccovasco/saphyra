################################################################################
# Package: prometheus
################################################################################

if($ENV{Athena_SETUP} STREQUAL "on")
  # Declare the package name:
  atlas_subdir( saphyra )
  # Install files from the package:
  atlas_install_python_modules( python/*.py )
else()

  gaugi_install_python_modules( ${CMAKE_CURRENT_SOURCE_DIR}/python saphyra)
  gaugi_install_scripts( ${CMAKE_CURRENT_SOURCE_DIR}/scripts/standalone)
  gaugi_install_scripts( ${CMAKE_CURRENT_SOURCE_DIR}/scripts/grid_scripts)
endif()

