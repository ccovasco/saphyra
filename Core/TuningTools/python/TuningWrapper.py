__all__ = ['TuningWrapper']

import numpy as np
from Gaugi import ( Logger, LoggingLevel, NotSet, checkForUnusedVars
                       , retrieve_kw, firstItemDepth, expandFolders )
from Gaugi.messenger.macros import *
from TuningTools.coreDef      import coreConf, npCurrent, TuningToolCores
from TuningTools.TuningJob    import ReferenceBenchmark,   ReferenceBenchmarkCollection, BatchSizeMethod
from TuningTools.dataframe.EnumCollection     import Dataset, RingerOperation
from TuningTools.DataCurator  import CuratedSubset
from TuningTools.Neural import Neural, DataTrainEvolution, Roc

def _checkData(data,target=None):
  if not npCurrent.check_order(data):
    raise TypeError('order of numpy data is not fortran!')
  if target is not None and not npCurrent.check_order(target):
    raise TypeError('order of numpy target is not fortran!')


class TuningWrapper(Logger):
  """
    TuningTool is the higher level representation of the TuningToolPyWrapper class.
  """
  # FIXME Create a dict with default options for FastNet and for ExMachina
  def __init__( self, dataCurator, **kw ):
    Logger.__init__( self, kw )
    self.references = ReferenceBenchmarkCollection( [] )
    coreframe = coreConf.core_framework()
    self.dataCurator = dataCurator

    self._level                = retrieve_kw( kw, 'level',          LoggingLevel.INFO               )
    epochs                     = retrieve_kw( kw, 'epochs',                 10000                   )
    maxFail                    = retrieve_kw( kw, 'maxFail',                5                      )
    self.doPerf                = retrieve_kw( kw, 'doPerf',                 True                    )
    self.useTstEfficiencyAsRef = retrieve_kw( kw, 'useTstEfficiencyAsRef',  False                   )
    self._saveOutputs          = retrieve_kw( kw, 'saveOutputs',            False                   )
    self.summaryOPs            = retrieve_kw( kw, 'summaryOPs',            [None for _ in range(len(self.dataCurator.dataLocation))])

    self.batchMethod           = BatchSizeMethod.retrieve( retrieve_kw( kw, 'batchMethod', BatchSizeMethod.MinClassSize \
                                                           if not 'batchSize' in kw or kw['batchSize'] is NotSet else
                                                           BatchSizeMethod.Manual) )
    self._merged = retrieve_kw( kw, 'merged', False)
    showEvo  = retrieve_kw( kw, 'showEvo',       50 )
    ###############################################################################################
    ###cadu shit1
    from keras import callbacks
    #from TuningTools.keras_util.callbacks import PerformanceHistory
    self._earlyStopping = callbacks.EarlyStopping( monitor='val_loss' # val_acc
                                               , patience=maxFail
                                               , verbose=2
                                               , mode='auto')
    #self._historyCallback = PerformanceHistory( display = retrieve_kw( kw, 'showEvo', 50 ) )
    ###############################################################################################

    if coreConf() is TuningToolCores.FastNet:
      seed = retrieve_kw( kw, 'seed', None )
      self._core = coreframe( level = LoggingLevel.toC(self._level),seed = seed )
      self._core.trainFcn           = retrieve_kw( kw, 'optmin_alg' , 'trainrp' )
      self._core.showEvo            = showEvo
      self._core.multiStop          = retrieve_kw( kw, 'doMultiStop',   True      )
      self.addPileupToOutputLayer   = retrieve_kw( kw, 'addPileupToOutputLayer',   False      )
      self._core.epochs             = epochs
      self._core.maxFail            = maxFail
      if self.addPileupToOutputLayer: self.dataCurator.addPileupToOutputLayer = True


    # TODO Add properties
    elif coreConf() is TuningToolCores.keras:
      from keras.optimizers import RMSprop, SGD, Adam
      self._core = coreframe(
                              optmin_alg    = retrieve_kw( kw, 'optmin_alg' , Adam(lr=0.005,beta_1=0.9,beta_2=0.999,epsilon=1e-08) ),
                              #optmin_alg    = retrieve_kw( kw, 'optmin_alg' , RMSprop(lr=0.001, rho=0.9, epsilon=1e-08) ),
                              secondaryPP   = retrieve_kw( kw, 'secondaryPP'          ,  None                           ),
                              costFunction  = retrieve_kw( kw, 'costFunction'         , 'mean_squared_error'            ), # 'binary_crossentropy'
                              metrics       = retrieve_kw( kw, 'metrics'              , ['accuracy', ]                  ),
                              shuffle       = retrieve_kw( kw, 'shuffle'              , True                            ),
                              multiStop     = retrieve_kw( kw, 'doMultiStop'          , True                            ),
                              epochs        = epochs,
                              nFails        = 25, # maxFail
                              showEvo       = showEvo,
                              level         = self._level,

                              )
    else:
      MSG_FATAL(self, "TuningWrapper not implemented for %s", coreConf)

    self.batchSize             = retrieve_kw( kw, 'batchSize',              NotSet                  )
    checkForUnusedVars(kw, self._warning )
    del kw

    # Set default empty values:
    if coreConf() is TuningToolCores.keras:
      self._emptyData  = npCurrent.fp_array([])
    elif coreConf() is TuningToolCores.FastNet:
      self._emptyData = list()
    self._emptyHandler = None
    if coreConf() is TuningToolCores.keras:
      self._emptyTarget = npCurrent.fp_array([[]]).reshape(
              npCurrent.access( pidx=1,
                                oidx=0 ) )
    elif coreConf() is TuningToolCores.FastNet:
      self._emptyTarget = None


    # Set holders:
    self._trnData    = self._emptyData
    self._valData    = self._emptyData
    self._tstData    = self._emptyData
    self._trnHandler = self._emptyHandler
    self._valHandler = self._emptyHandler
    self._tstHandler = self._emptyHandler
    self._trnTarget  = self._emptyTarget
    self._valTarget  = self._emptyTarget
    self._tstTarget  = self._emptyTarget
    self.sortIdx = None
    self.addPileupToOutputLayer = False
  # TuningWrapper.__init__


  def release(self):
    """
    Release holden data, targets and handlers.
    """
    self._trnData = self._emptyData
    self._trnHandler = self._emptyHandler
    self._trnTarget = self._emptyTarget

  @property
  def batchSize(self):
    """
    External access to batchSize
    """
    if coreConf() is TuningToolCores.keras:
      return self._core.batchSize
    elif coreConf() is TuningToolCores.FastNet:
      return self._core.batchSize


  @batchSize.setter
  def batchSize(self, val):
    """
    External access to batchSize
    """
    if val is not NotSet:
      self.batchMethod = BatchSizeMethod.Manual
      if coreConf() is TuningToolCores.keras:
        self._core.batchSize = val
      elif coreConf() is TuningToolCores.FastNet:
        self._core.batchSize   = val
      MSG_DEBUG(self, 'Set batchSize to %d', val )


  def __batchSize(self, val):
    """
    Internal access to batchSize
    """
    if coreConf() is TuningToolCores.keras:
      self._core.batchSize = val
    elif coreConf() is TuningToolCores.FastNet:
      self._core.batchSize   = val
    MSG_DEBUG(self, 'Set batchSize to %d', val )

  @property
  def doMultiStop(self):
    """
    External access to doMultiStop
    """
    if coreConf() is TuningToolCores.keras:
      return False
    elif coreConf() is TuningToolCores.FastNet:
      return self._core.multiStop


  def setReferences(self, references):
    # Make sure that the references are a collection of ReferenceBenchmark
    references = ReferenceBenchmarkCollection(references)
    if len(references) == 0:
      MSG_FATAL(self, "Reference collection must be not empty!", ValueError)

    if coreConf() is TuningToolCores.FastNet:
      if self.doMultiStop:
        self.references = ReferenceBenchmarkCollection( [None] * 3 )
        # This is done like this for now, to prevent adding multiple
        # references. However, this will be removed when the FastNet is
        # made compatible with multiple references
        retrievedSP = False
        retrievedPD = False
        retrievedPF = False
        for ref in references:
          if ref.reference is ReferenceBenchmark.SP:
            if not retrievedSP:
              retrievedSP = True
              self.references[0] = ref
            else:
              MSG_WARNING(self, "Ignoring multiple Reference object: %s", ref)
          elif ref.reference is ReferenceBenchmark.Pd:
            if not retrievedPD:
              retrievedPD = True
              self.references[1] = ref
              self._core.det = self.references[1].getReference()
            else:
              MSG_WARNING(self, "Ignoring multiple Reference object: %s", ref)
          elif ref.reference is ReferenceBenchmark.Pf:
            if not retrievedPF:
              retrievedPF = True
              self.references[2] = ref
              self._core.fa = self.references[2].getReference()
            else:
              MSG_WARNING(self, "Ignoring multiple Reference object: %s", ref)
        MSG_INFO(self, 'Set multiStop target [Sig_Eff(%%) = %r, Bkg_Eff(%%) = %r].',
                        self._core.det * 100.,
                        self._core.fa * 100.  )
      else:
        if len(references) != 1:
          MSG_INFO(self, "Ignoring other references when using FastNet with MSE.")
          references = references[:1]
        self.references = references
        ref = self.references[0]
        MSG_INFO(self, "Set single reference target to: %s", self.references[0])


    elif coreConf() is TuningToolCores.keras:
      self.references = references


  def setSortIdx(self, sort):

    if coreConf() is TuningToolCores.FastNet:
      if self.doMultiStop and self.useTstEfficiencyAsRef:
        if not len(self.references) == 3 or  \
            not self.references[0].reference == ReferenceBenchmark.SP or \
            not self.references[1].reference == ReferenceBenchmark.Pd or \
            not self.references[2].reference == ReferenceBenchmark.Pf:
          MSG_FATAL(self, "The tuning wrapper references are not correct!")
        self.sortIdx = sort
        self._core.det = self.references[1].getReference( ds = Dataset.Validation, sort = sort )
        self._core.fa = self.references[2].getReference( ds = Dataset.Validation, sort = sort )
        MSG_INFO(self, 'Set multiStop target [sort:%d | Sig_Eff(%%) = %r, Bkg_Eff(%%) = %r].',
                          sort,
                          self._core.det * 100.,
                          self._core.fa * 100.  )


  def trnData(self, release = False):
    if coreConf() is TuningToolCores.keras:
      ret = [self.__separete_patterns(ds,self._trnTarget) for ds in self._trnData] if self._merged else \
             self.__separete_patterns(self._trnData, self._trnTarget)
    else:
      ret = self._trnData
    if release: self.release()
    return ret


  def setTrainData(self, data, target=None):
    """
      Set test dataset of the tuning method.
    """
    if self._merged:
      self._sgnSize = data[0][0].shape[npCurrent.odim]
      self._bkgSize = data[0][1].shape[npCurrent.odim]

      if coreConf() is TuningToolCores.keras:
        self._trnData = list()
        for pat in data:
          if target is None:
            ds, target = self.__concatenate_patterns(pat)
          else:
            ds, _ = self.__concatenate_patterns(pat)
          _checkData(ds, target)
          self._trnData.append(ds)
        self._trnTarget = target
      elif coreConf() is TuningToolCores.FastNet:
        MSG_FATAL(self,  "Expert Neural Networks not implemented for FastNet core" )
    else:
      self._sgnSize = data[0].shape[npCurrent.odim]
      self._bkgSize = data[1].shape[npCurrent.odim]
      if coreConf() is TuningToolCores.keras:
        if target is None:
          data, target = self.__concatenate_patterns(data)
        _checkData(data, target)
        self._trnData = data
        self._trnTarget = target

      elif coreConf() is TuningToolCores.FastNet:
        self._trnData = data
        self._core.setTrainData( data )


  def valData(self, release = False):
    if coreConf() is TuningToolCores.keras:
      ret = [self.__separete_patterns(ds,self._valTarget) for ds in self._valData] if self._merged else \
             self.__separete_patterns(self._valData, self._valTarget)
    else:
      ret = self._valData
    if release: self.release()
    return ret


  def setValData(self, data, target=None):
    """
      Set test dataset of the tuning method.
    """
    if self._merged:
      if coreConf() is TuningToolCores.keras:
        self._valData = list()
        for pat in data:
          if target is None:
            ds, target = self.__concatenate_patterns(pat)
          else:
            ds, _ = self.__concatenate_patterns(pat)
          _checkData(ds, target)
          self._valData.append(ds)
        self._valTarget = target
      elif coreConf() is TuningToolCores.FastNet:
        MSG_FATAL(self,  "Expert Neural Networks not implemented for FastNet core" )
    else:
      if coreConf() is TuningToolCores.keras:
        if target is None:
          data, target = self.__concatenate_patterns(data)
        _checkData(data, target)
        self._valData = data
        self._valTarget = target
      elif coreConf() is TuningToolCores.FastNet:
        self._valData = data
        self._core.setValData( data )


  def testData(self, release = False):
    if coreConf() is TuningToolCores.keras:
      ret = [self.__separete_patterns(ds,self._tstTarget) for ds in self._tstData] if self._merged else \
             self.__separete_patterns(self._tstData, self._tstTarget)
    else:
      ret = self._tstData
    if release: self.release()
    return ret


  def setTestData(self, data, target=None):
    """
      Set test dataset of the tuning method.
    """
    if self._merged:
      if coreConf() is TuningToolCores.keras:
        self._tstData = list()
        for pat in data:
          if target is None:
            ds, target = self.__concatenate_patterns(pat)
          else:
            ds, _ = self.__concatenate_patterns(pat)
          _checkData(ds, target)
          self._tstData.append(ds)
        self._tstTarget = target
      elif coreConf() is TuningToolCores.FastNet:
        MSG_FATAL(self,  "Expert Neural Networks not implemented for FastNet core" )
    else:
      if coreConf() is TuningToolCores.keras:
        if target is None:
          data, target = self.__concatenate_patterns(data)
        _checkData(data, target)
        self._tstData = data
        self._tstTarget = target
      elif coreConf() is TuningToolCores.FastNet:
        self._tstData = data
        self._core.setTstData( data )


  def newff(self, nodes, funcTrans = NotSet, model = None):
    """
      Creates new feedforward neural network
    """
    MSG_DEBUG(self, 'Initalizing newff...')
    if coreConf() is TuningToolCores.FastNet:
      if funcTrans is NotSet: funcTrans = ['tansig', 'tansig']
      if not self._core.newff(nodes, funcTrans, self._core.trainFcn):
        MSG_FATAL(self, "Couldn't allocate new feed-forward!")
    elif coreConf() is TuningToolCores.keras:
      if not model:
        MSG_FATAL(self, "Keras model must be passed as argument in newff function.")
      self._core.compile(model)

  #####################################################################################
  ###cadu shit 12.1
  def deepff(self, nodes,hidden_neurons,layers_weights,layers_config, funcTrans = NotSet):
    """
      Creates new feedforward neural network
    """
    self._debug('Initalizing newff...')
    #if coreConf() is TuningToolCores.ExMachina:
    #  if funcTrans is NotSet: funcTrans = ['tanh', 'tanh']
    #  self._model = self._core.FeedForward(nodes, funcTrans, 'nw')
    if coreConf() is TuningToolCores.FastNet:
      if funcTrans is NotSet: funcTrans = ['tansig', 'tansig']
      if not self._core.newff(nodes, funcTrans, self._core.trainFcn):
        self._fatal("Couldn't allocate new feed-forward!")
    elif coreConf() is TuningToolCores.keras:
      self._fine_tuning='yes'
      from keras.models import Sequential
      from keras.layers.core import Dense, Dropout, Activation
      model = Sequential()
      for i_hn in range(len(hidden_neurons)):
        print i_hn
        #weight = layers_weights[i_hn][0]
        weight = layers_weights[i_hn][:2]
        print weight[0].shape,weight[1].shape
        if i_hn == 0:
          model.add(Dense(hidden_neurons[0],input_dim=nodes[0],weights=weight,trainable=True))
          model.add(Activation('tanh'))
          #model.add(Dropout(rate=0.5))
        else:
          model.add(Dense(hidden_neurons[i_hn],weights=weight,trainable=True))
          model.add(Activation('tanh'))
          #model.add(Dropout(rate=0.5))
        #print weights[0].shape,weights[1].shape,weights[2].shape,weights[3].shape
        #config = layers_config[i_hn]
        #model = Sequential.from_config(config)
        #model.set_weights(weights)
        #model.layers.pop()
        #model.layers.pop()
        #model.summary()
        last_hl = hidden_neurons[i_hn]
      model.add( Dense( last_hl
                       #, input_dim=nodes[0]
                       , init='identity'
                       , trainable=False
                       , name='dense_last_hl' ) )
      model.add( Activation('linear') )
      #model.add( Dense( nodes[1]
      #                , input_dim=nodes[0]
      #                , init='uniform'
      #                , name='dense_last_hl' ) )
      #model.add( Activation('tanh') )]
      from keras.optimizers import Adam
      model.add( Dense( nodes[2], init='uniform', name='dense_output' ) )
      model.add( Activation('tanh') )
      model.compile( loss='mean_squared_error'
                   , optimizer = Adam(lr=0.005,beta_1=0.9,beta_2=0.999,epsilon=1e-08)
                   , metrics = ['accuracy', ] )
      # #keras.callbacks.History()
      # #keras.callbacks.ModelCheckpoint(filepath, monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)
      print model.summary()
      self._model = model
      #self._historyCallback.model = model

  def deepff2(self, nodes, funcTrans = NotSet):
    """
      Creates new feedforward neural network
    """
    self._debug('Initalizing deepff...')
    models = {}
    #if coreConf() is TuningToolCores.ExMachina:
    #  if funcTrans is NotSet: funcTrans = ['tanh', 'tanh']
    #  self._model = self._core.FeedForward(nodes, funcTrans, 'nw')
    if coreConf() is TuningToolCores.FastNet:
      if funcTrans is NotSet: funcTrans = ['tansig', 'tansig']
      if not self._core.newff(nodes, funcTrans, self._core.trainFcn):
        self._fatal("Couldn't allocate new feed-forward!")
    elif coreConf() is TuningToolCores.keras:
      from keras.models import Sequential
      from keras.layers.core import Dense, Dropout, Activation
      self._fine_tuning= 'no'
      self._info("Using Keras")
      model = Sequential()
      model.add( Dense( nodes[0]
                        , input_dim=nodes[0]
                        , init='identity'
                        , trainable=False
                        , name='dense_last_hl' ) )
      model.add( Activation('linear') )
      #model.add( Dense( 20 , input_dim=nodes[0] , init='uniform' ) )
      #model.add( Activation('tanh') )

      # model.add( Dense( 5 #nodes[1]
                        # , input_dim=nodes[0]
                        # , init='uniform'
                        # , name='dense_last_hl' ) )
      # model.add( Activation('tanh', name='Activation_last_hl'))

      #model.add(Dropout(0.5))
      from keras.optimizers import Adam
      model.add( Dense( nodes[2], init='uniform', name='dense_output' ) )
      model.add( Activation('tanh',name='Activation_output'))
      model.compile( loss='mean_squared_error'
                       , optimizer = Adam(lr=0.005,beta_1=0.9,beta_2=0.999,epsilon=1e-08)
                       , metrics = ['accuracy', ] )
      # keras.callbacks.History()
      # keras.callbacks.ModelCheckpoint(filepath, monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)
      self._model = model
      #self._historyCallback.model = model
      #self._historyCallback.model = model
      print model.summary()
      #self._core.compile(model)
  #####################################################################################

  def train_c(self):
    """
      Train feedforward neural network
    """
    self.setSortIdx( self.dataCurator.sort )
    from copy import deepcopy
    # Holder of the discriminators:
    tunedDiscrList = []
    tuningInfo = {}

    # Set batch size:
    if self.batchMethod is BatchSizeMethod.MinClassSize:
      self.__batchSize( self._bkgSize if self._sgnSize > self._bkgSize else self._sgnSize )
    elif self.batchMethod is BatchSizeMethod.HalfSizeSignalClass:
      self.__batchSize( self._sgnSize // 2 )
    elif self.batchMethod is BatchSizeMethod.OneSample:
      self.__batchSize( 1 )

    rawDictTempl = { 'discriminator'  : None,
                     'benchmark'      : None }

    if coreConf() is TuningToolCores.keras:

      history = self._core.fit( self._trnData, self._trnTarget, self._valData, self._valTarget )
      # Retrieve raw network
      rawDictTempl['discriminator'] = self.__discr_to_dict( self._core.model )
      rawDictTempl['benchmark'] = self.references[0]
      rawDictTempl['keras_summary'] = {'history':history}
      tunedDiscrList.append( deepcopy( rawDictTempl ) )
      tuningInfo = DataTrainEvolution( history ).toRawObj()


    elif coreConf() is TuningToolCores.FastNet:
      self._debug('executing train_c')
      [discriminatorPyWrapperList, trainDataPyWrapperList] = self._core.train_c()
      self._debug('finished train_c')
      # Transform model tolist of  dict

      if self.doMultiStop:
        for idx, discr in enumerate( discriminatorPyWrapperList ):
          rawDictTempl['discriminator'] = self.__discr_to_dict( discr )
          rawDictTempl['benchmark'] = self.references[idx]
          # FIXME This will need to be improved if set to tune for multiple
          # Pd and Pf values.
          tunedDiscrList.append( deepcopy( rawDictTempl ) )
      else:
        rawDictTempl['discriminator'] = self.__discr_to_dict( discriminatorPyWrapperList[0] )
        rawDictTempl['benchmark'] = self.references[0]
        if self.useTstEfficiencyAsRef and self.sortIdx is not None:
          rawDictTempl['sortIdx'] = self.sortIdx
        tunedDiscrList.append( deepcopy( rawDictTempl ) )
      tuningInfo = DataTrainEvolution( trainDataPyWrapperList ).toRawObj()
      # TODO
    # cores

    # Retrieve performance:
    for idx, tunedDiscrDict in enumerate(tunedDiscrList):
      discr = tunedDiscrDict['discriminator']
      if self.doPerf:
        self._debug('Retrieving performance.')

        if coreConf() is TuningToolCores.keras:
          opRoc, tstRoc = self._core.evaluate( self._trnData, self._trnTarget, self._valData, self._valTarget, self._tstData, self._tstTarget)

        elif coreConf() is TuningToolCores.FastNet:
          perfList = self._core.valid_c( discriminatorPyWrapperList[idx] )
          tstRoc = Roc( perfList[0] )
          opRoc = Roc( perfList[1] )
          #trnRoc( perfList[0] )
        # Add rocs to output information

        # TODO Change this to raw object
        tunedDiscrDict['summaryInfo'] = { 'roc_operation' : opRoc.toRawObj(),
                                          'roc_test'      : tstRoc.toRawObj(),
                                          }

        for ref in self.references:
          if coreConf() is TuningToolCores.FastNet:
            # FastNet won't loop on this, this is just looping for keras right now
            ref = tunedDiscrDict['benchmark']
          # Print information:
          tstPoint = tstRoc.retrieve( ref )
          self._info( 'Test (%s): sp = %f, pd = %f, pf = %f, thres = %r'
                    , ref.name
                    , tstPoint.sp_value
                    , tstPoint.pd_value
                    , tstPoint.pf_value
                    , tstPoint.thres_value )
          opPoint = opRoc.retrieve( ref )
          self._info( 'Operation (%s): sp = %f, pd = %f, pf = %f, thres = %r'
                    , ref.name
                    , opPoint.sp_value
                    , opPoint.pd_value
                    , opPoint.pf_value
                    , opPoint.thres_value )



    self._debug("Finished train_c on python side.")

    return tunedDiscrList, tuningInfo
  # end of train_c

  ###############################################################################
  ###cadu shit12.2
  def trainC_Deep( self, fname=None, short_name=None ):
    """
      Train expert feedforward neural network
    """
    #if coreConf() is TuningToolCores.ExMachina:
    #  self._fatal( "Expert Neural Networks not implemented for ExMachina" )
    if coreConf() is TuningToolCores.FastNet:
      self._fatal( "Expert Neural Networks not implemented for FastNet" )
    elif coreConf() is TuningToolCores.keras:
      from copy import deepcopy

      # Set batch size:
      if self.batchMethod is BatchSizeMethod.MinClassSize:
        self.__batchSize( self._bkgSize if self._sgnSize > self._bkgSize else self._sgnSize )
      elif self.batchMethod is BatchSizeMethod.HalfSizeSignalClass:
        self.__batchSize( self._sgnSize // 2 )
      elif self.batchMethod is BatchSizeMethod.OneSample:
        self.__batchSize( 1 )

      #references = ['SP','Pd','Pf']

      # Holder of the discriminators:
      tunedDiscrList = []
      tuningInfo = {}

      #for idx, ref in enumerate(references):
      #rawDictTempl = { 'discriminator' : None,
      #                 'benchmark' : None }
      ##########################################################
      ##APAGAR

      import time
      import datetime
      start_run = time.time()
      import keras
      nbatch_size = 2048 #self.batchSize

      tbCallBack = keras.callbacks.TensorBoard(log_dir='/home/caducovas/tensorboard/classError/Discriminator_'+short_name+'_batch_size_'+str(nbatch_size), histogram_freq=30, write_graph=True, write_images=False,write_grads=True,update_freq='batch')
      checkpoints = keras.callbacks.ModelCheckpoint(fname+'/models/'+short_name+'.h5', monitor='val_loss', verbose=0, save_best_only=True, save_weights_only=False, mode='auto', period=1)

      print 'WRAPPER DDMF'
      print type(self._trnData), type(self._trnTarget), type(self._valData), type(self._valTarget), type(self._tstData), type(self._tstTarget)
      print self._trnData.shape, self._trnTarget.shape, self._valData.shape, self._valTarget.shape, self._tstData.shape, self._tstTarget.shape
      print np.unique(self._trnTarget), np.unique(self._valTarget), np.unique(self._tstTarget)
      ########################################################
      print self._trnTarget
      #print 'transform'
      #self._trnTarget[self._trnTarget==-1] = 0
      #self._valTarget[self._valTarget==-1] = 0
      print self._trnTarget
      history = self._model.fit( self._trnData
                                    , self._trnTarget
                                    , epochs          = 10000
                                    , batch_size      = nbatch_size #1024 #self.batchSize
                                    #, callbacks       = [self._historyCallback, self._earlyStopping]
                                    , callbacks       = [self._earlyStopping, checkpoints]
                                    #, callbacks       = [self._earlyStopping]
                                    , verbose         = 1
                                    , validation_data = ( self._valData , self._valTarget )
                                    , shuffle         = True
                                    )

      end_run = time.time()
      print 'Model '+short_name+' took '+ str(datetime.timedelta(seconds=(end_run - start_run))) +' to finish.'

      import tensorflow as tf

      from keras.models import load_model
      self._model = load_model(fname+'/models/'+short_name+'.h5')

      #(self._model.save(
    #          '%s.h5'%(fname+'/models/'+short_name)))

      rawDictTempl = { 'discriminator': None,
                       'benchmark': None }
      for idx,ref in enumerate(self.references):
        print self.references[idx]
        # Retrieve raw network
        rawDictTempl['discriminator'] = self.__discr_to_dict( self._model )
        rawDictTempl['benchmark'] = self.references[idx]
        tunedDiscrList.append( deepcopy( rawDictTempl ) )
      tuningInfo = DataTrainEvolution( history ).toRawObj()

      try:
        from sklearn.metrics import roc_curve
      except ImportError:
        # FIXME Can use previous function that we used here as an alternative
        raise ImportError("sklearn is not available, please install it.")

      # Retrieve performance:
      #@@opRoc, tstRoc = Roc(), Roc()

      for idx, tunedDiscrDict in enumerate(tunedDiscrList):
        opPoints=[]
        tstPoints=[]
        refName=[]
        discr = tunedDiscrDict['discriminator']
        if self.doPerf:
          self._debug('Retrieving performance...')
          # propagate inputs:
          trnOutput = self._model.predict(self._trnData)
          valOutput = self._model.predict(self._valData)
          tstOutput = self._model.predict(self._tstData) if self._tstData else npCurrent.fp_array([])
          try:
            allOutput = np.concatenate([trnOutput,valOutput,tstOutput] )
            allTarget = np.concatenate([self._trnTarget,self._valTarget, self._tstTarget] )
          except ValueError:
            allOutput = np.concatenate([trnOutput,valOutput] )
            allTarget = np.concatenate([self._trnTarget,self._valTarget] )
          # Retrieve Rocs:
          #opRoc(valOutput,self._valTarget)
          opRoc = Roc( allOutput, allTarget )
          if self._tstData: tstRoc = Roc( tstOutput, self._tstTarget )
          #tstRoc( tstOutput, self._tstTarget )
          else: tstRoc = Roc( valOutput, self._valTarget )
          #np.savez_compressed(fname+'/results/train_all_target_official'+short_name,self._trnTarget)
          #np.savez_compressed(fname+'/results/val_all_target_official'+short_name,self._valTarget)
          # Add rocs to output information
          # TODO Change this to raw object
          #if coreConf() is TuningToolCores.keras:
            #opRoc, tstRoc = self._core.evaluate( self._trnData, self._trnTarget, self._valData, self._valTarget, self._tstData, self._tstTarget)
          tunedDiscrDict['summaryInfo'] = { 'roc_operation' : opRoc.toRawObj(),
                                            'roc_test' : tstRoc.toRawObj() }

          for ref2 in self.references:
            opPoint = opRoc.retrieve( ref2 )
            tstPoint = tstRoc.retrieve( ref2 )

            opPoints.append([ref2.name,opPoint])
            tstPoints.append([ref2.name,tstPoint])
            refName.append(ref2.name)

            print ref2.name,ref2.refVal,opPoint.sp_value,opPoint.pd_value,opPoint.pf_value, opPoint.thres_value,ref2
            # Print information:
            self._info( 'Operation (%s): sp = %f, pd = %f, pf = %f, thres = %f'
                      , ref2.name
                      , opPoint.sp_value
                      , opPoint.pd_value
                      , opPoint.pf_value
                      , opPoint.thres_value )
            self._info( 'Test (%s): sp = %f, pd = %f, pf = %f, thres = %f'
                      , ref2.name
                      , tstPoint.sp_value
                      , tstPoint.pd_value
                      , tstPoint.pf_value
                      , tstPoint.thres_value )
      self._info("Finished trainC_Deep")
    print 'len self reference', len(self.references) #[0]
    opPoint=opRoc.retrieve(self.references[0])
    tstPoint=tstRoc.retrieve(self.references[0])
    self._debug("Finished trainC_Deep on python side.")

    #import dataset
    #db = dataset.connect('sqlite:////scratch/22061a/caducovas/run/mydatabase.db')
    #table= db['roc'] =
    #tf.reset_default_graph()
    print type(opPoints),type(tstPoints)
    print len(opPoints), len(tstPoints)
    return tunedDiscrList, tuningInfo, history,self._model,self._valTarget,valOutput,self._trnTarget,trnOutput,opPoints,tstPoints,self._fine_tuning,refName
  # end of trainC_Deep



  def trainC_Models( self ):
    """
     Train expert feedforward neural network
    """
    if coreConf() is TuningToolCores.ExMachina:
        self._fatal( "Expert Neural Networks not implemented for ExMachina" )
    elif coreConf() is TuningToolCores.FastNet:
        self._fatal( "Expert Neural Networks not implemented for FastNet" )
    elif coreConf() is TuningToolCores.keras:
        from copy import deepcopy

    #Set batch size:
    if self.batchMethod is BatchSizeMethod.MinClassSize:
        self.__batchSize( self._bkgSize if self._sgnSize > self._bkgSize else self._sgnSize )
    elif self.batchMethod is BatchSizeMethod.HalfSizeSignalClass:
        self.__batchSize( self._sgnSize // 2 )
    elif self.batchMethod is BatchSizeMethod.OneSample:
        self.__batchSize( 1 )

    #references = ['SP','Pd','Pf']

    # Holder of the discriminators:
    tunedDiscrList = []
    tuningInfo = {}

    import sys
    sys.path.insert(0,'/home/caducovas/DataMining')
    from analysis_functions import gaussian_naive_bayes,log_reg,perceptron,nearest_neighbours,decision_tree,random_forest, ada_boost,linear_discriminant_analysis,quadratic_discriminant_analysis,svm,linear_svm
    #for idx, ref in enumerate(references):
    #rawDictTempl = { 'discriminator' : None,
    #              'benchmark' : None }
    ##########################################################
    ##APAGAR
    print 'WRAPPER DDMF'
    print type(self._trnData), type(self._trnTarget), type(self._valData), type(self._valTarget), type(self._tstData), type(self._tstTarget)
    print self._trnData.shape, self._trnTarget.shape, self._valData.shape, self._valTarget.shape, self._tstData.shape, self._tstTarget.shape
    print np.unique(self._trnTarget), np.unique(self._valTarget), np.unique(self._tstTarget)

    self._valTarget[self._valTarget==-1] = 0
    self._trnTarget[self._trnTarget==-1] = 0

    print np.unique(self._trnTarget), np.unique(self._valTarget), np.unique(self._tstTarget)
    ########################################################
    #history = self._model.fit( self._trnData
    #                        , self._trnTarget
    #                        , epochs       = self.trainOptions['nEpochs']
    #                        , batch_size      = self.batchSize
    #                        #, callbacks    = [self._historyCallback, self._earlyStopping]
    #                        , callbacks    = [self._earlyStopping]
    #                        , verbose      = 2
    #                        , validation_data = ( self._valData , self._valTarget )
    #                        , shuffle      = self.trainOptions['shuffle']
    #                        )
    predTest,predTrain,self._model = log_reg(self._trnData,self._trnTarget,self._valData,self._valTarget,compute_threshold=False)
    mname="log_reg"
    history = self._model
    rawDictTempl = { 'discriminator': None,
                  'benchmark': None }
    for idx,ref in enumerate(self.references):
      print self.references[idx]
      # Retrieve raw network
      rawDictTempl['discriminator'] = None #self.__discr_to_dict( self._model )
      rawDictTempl['benchmark'] = self.references[idx]
      tunedDiscrList.append( deepcopy( rawDictTempl ) )
    tuningInfo = None #DataTrainEvolution( history ).toRawObj()

    try:
      from sklearn.metrics import roc_curve
    except ImportError:
      # FIXME Can use previous function that we used here as an alternative
      raise ImportError("sklearn is not available, please install it.")

    # Retrieve performance:
    opRoc, tstRoc = Roc(), Roc()
    for idx, tunedDiscrDict in enumerate(tunedDiscrList):
      opPoints=[]
      tstPoints=[]
      refName=[]
      discr = tunedDiscrDict['discriminator']
      if self.doPerf:
        self._debug('Retrieving performance...')
        # propagate inputs:
        trnOutput = self._model.predict_proba(self._trnData)[:,1]
        valOutput = self._model.predict_proba(self._valData)[:,1]
        tstOutput = self._model.predict_proba(self._tstData)[:,1] if self._tstData else npCurrent.fp_array([])
        print 'classes', self._model.classes_

        print trnOutput.shape, valOutput.shape, tstOutput.shape #valOutput[:,0],valOutput[:,1]
        #try:
        #  allOutput = np.concatenate([trnOutput,valOutput,tstOutput] )
        #  allTarget = np.concatenate([self._trnTarget,self._valTarget, self._tstTarget] )
        #except ValueError:
        #  allOutput = np.concatenate([trnOutput,valOutput] )
        #  allTarget = np.concatenate([self._trnTarget,self._valTarget] )

        allOutput = np.concatenate([trnOutput,valOutput] )
        allTarget = np.concatenate([self._trnTarget,self._valTarget] )
        # Retrieve Rocs:
        #opRoc(valOutput,self._valTarget) #opRoc( allOutput, allTarget )

        from sklearn.metrics import roc_curve
        pfs,pds,ths = roc_curve(allTarget,allOutput,pos_label=1,drop_intermediate=False)
        print ths,len(ths),len(allTarget),len(allOutput)

        print trnOutput.shape,valOutput.shape,self._trnTarget.shape,self._valTarget.shape
        print allOutput.shape, allTarget.shape

        opRoc( allOutput, allTarget )
        if self._tstData: tstRoc( tstOutput, self._tstTarget )
        #tstRoc( tstOutput, self._tstTarget )
        else: tstRoc( valOutput, self._valTarget )
        # Add rocs to output information
        # TODO Change this to raw object
        tunedDiscrDict['summaryInfo'] = { 'roc_operation' : opRoc.toRawObj(),
                                    'roc_test' : tstRoc.toRawObj() }

        for ref2 in self.references:
          opPoint = opRoc.retrieve( ref2 )
          tstPoint = tstRoc.retrieve( ref2 )

          opPoints.append([ref2.name,opPoint])
          tstPoints.append([ref2.name,tstPoint])
          refName.append(ref2.name)
          # Print information:
          self._info( 'Operation (%s): sp = %f, pd = %f, pf = %f, thres = %f'
                 , ref2.name
                 , opPoint.sp_value
                 , opPoint.pd_value
                 , opPoint.pf_value
                 , opPoint.thres_value )
          self._info( 'Test (%s): sp = %f, pd = %f, pf = %f, thres = %f'
                 , ref2.name
                 , tstPoint.sp_value
                 , tstPoint.pd_value
                 , tstPoint.pf_value
                 , tstPoint.thres_value )
    self._info("Finished trainC_Deep")
    print self.references[0]
    opPoint=opRoc.retrieve(self.references[0])
    tstPoint=tstRoc.retrieve(self.references[0])
    self._debug("Finished trainC_Deep on python side.")
    #import dataset
    #db = dataset.connect('sqlite:////scratch/22061a/caducovas/run/mydatabase.db')
    #table= db['roc'] =
    print type(opPoint),type(tstPoint)
    return tunedDiscrList, tuningInfo, history,self._model,self._valTarget,valOutput,self._trnTarget,trnOutput,opPoints,tstPoints,mname,self._fine_tuning,refName
   # end of trainC_Deep

  def trainC_Deep_concat( self ):
    """
      Train expert feedforward neural network
    """
    if coreConf() is TuningToolCores.ExMachina:
      self._fatal( "Expert Neural Networks not implemented for ExMachina" )
    elif coreConf() is TuningToolCores.FastNet:
      self._fatal( "Expert Neural Networks not implemented for FastNet" )
    elif coreConf() is TuningToolCores.keras:
      from copy import deepcopy

      # Set batch size:
      if self.batchMethod is BatchSizeMethod.MinClassSize:
        self.__batchSize( self._bkgSize if self._sgnSize > self._bkgSize else self._sgnSize )
      elif self.batchMethod is BatchSizeMethod.HalfSizeSignalClass:
        self.__batchSize( self._sgnSize // 2 )
      elif self.batchMethod is BatchSizeMethod.OneSample:
        self.__batchSize( 1 )

      #references = ['SP','Pd','Pf']

      # Holder of the discriminators:
      tunedDiscrList = []
      tuningInfo = {}

      #for idx, ref in enumerate(references):
      #rawDictTempl = { 'discriminator' : None,
      #                 'benchmark' : None }
      ##########################################################
      ##APAGAR
      print 'WRAPPER DDMF'
      print type(self._trnData), type(self._trnTarget), type(self._valData), type(self._valTarget)
      print self._trnData.shape, self._trnTarget.shape, self._valData.shape, self._valTarget.shape, self._tstData.shape, self._tstTarget.shape
      print np.unique(self._trnTarget), np.unique(self._valTarget)
      ########################################################
      history = self._model.fit( [self._trnData[:,:88],self._trnData[:,88:]]#self._trnData
                                    , self._trnTarget
                                    , epochs          = 1000 #self.trainOptions['nEpochs']
                                    , batch_size      = self.batchSize
                                    #, callbacks       = [self._historyCallback, self._earlyStopping]
                                    , callbacks       = [self._earlyStopping]
                                    , verbose         = 2
                                    , validation_data = ( [self._valData[:,:88],self._valData[:,88:]] , self._valTarget )
                                    , shuffle         = self.trainOptions['shuffle']
                                    )

      rawDictTempl = { 'discriminator': None,
                       'benchmark': None }
      for idx,ref in enumerate(self.references):
        print self.references[idx]
        # Retrieve raw network
        rawDictTempl['discriminator'] = self.__discr_to_dict( self._model )
        rawDictTempl['benchmark'] = self.references[idx]
        tunedDiscrList.append( deepcopy( rawDictTempl ) )
      tuningInfo = DataTrainEvolution( history ).toRawObj()

      try:
        from sklearn.metrics import roc_curve
      except ImportError:
        # FIXME Can use previous function that we used here as an alternative
        raise ImportError("sklearn is not available, please install it.")

      # Retrieve performance:
      opRoc, tstRoc = Roc(), Roc()
      for idx, tunedDiscrDict in enumerate(tunedDiscrList):
        discr = tunedDiscrDict['discriminator']
        if self.doPerf:
          self._debug('Retrieving performance...')
          # propagate inputs:
          trnOutput = self._model.predict([self._trnData[:,:88],self._trnData[:,88:]])
          valOutput = self._model.predict([self._valData[:,:88],self._valData[:,88:]])
          tstOutput = self._model.predict([self._tstData[:,:88],self._tstData[:,88:]]) if self._tstData else npCurrent.fp_array([])
          try:
            allOutput = np.concatenate([trnOutput,valOutput,tstOutput] )
            allTarget = np.concatenate([self._trnTarget,self._valTarget, self._tstTarget] )
          except ValueError:
            allOutput = np.concatenate([trnOutput,valOutput] )
            allTarget = np.concatenate([self._trnTarget,self._valTarget] )
          # Retrieve Rocs:
          opRoc(trnOutput,self._trnTarget) #opRoc( allOutput, allTarget )
          if self._tstData: tstRoc( tstOutput, self._tstTarget )
          else: tstRoc( valOutput, self._valTarget )
          # Add rocs to output information
          # TODO Change this to raw object
          tunedDiscrDict['summaryInfo'] = { 'roc_operation' : opRoc.toRawObj(),
                                            'roc_test' : tstRoc.toRawObj() }

          for ref2 in self.references:
            opPoint = opRoc.retrieve( ref2 )
            tstPoint = tstRoc.retrieve( ref2 )
            # Print information:
            self._info( 'Operation (%s): sp = %f, pd = %f, pf = %f, thres = %f'
                      , ref2.name
                      , opPoint.sp_value
                      , opPoint.pd_value
                      , opPoint.pf_value
                      , opPoint.thres_value )
            self._info( 'Test (%s): sp = %f, pd = %f, pf = %f, thres = %f'
                      , ref2.name
                      , tstPoint.sp_value
                      , tstPoint.pd_value
                      , tstPoint.pf_value
                      , tstPoint.thres_value )
      self._info("Finished trainC_Deep")
    print self.references[0]
    opPoint=opRoc.retrieve(self.references[0])
    tstPoint=tstRoc.retrieve(self.references[0])
    self._debug("Finished trainC_Deep on python side.")

    #import dataset
    #db = dataset.connect('sqlite:////scratch/22061a/caducovas/run/mydatabase.db')
    #table= db['roc'] =
    return tunedDiscrList, tuningInfo, history,self._model,self._valTarget,valOutput,self._trnTarget,trnOutput,opPoint,tstPoint,self._fine_tuning
  # end of trainC_Deep
  ###############################################################################


  def __discr_to_dict(self, model):
    """
    Transform discriminators to dictionary
    """
    if coreConf() is TuningToolCores.keras:
      import json
      discrDict = {
                    'model':  json.loads(model.to_json()),
                    'weights': model.get_weights(),
                  }

    elif coreConf() is TuningToolCores.FastNet:
      n = []; w = []; b = [];
      for l in range( model.getNumLayers() ):
        n.append( model.getNumNodes(l) )
      for l in range( len(n) - 1 ):
        for j in range( n[l+1] ):
          for k in range( n[l] ):
            w.append( model.getWeight(l,j,k) )
          b.append( model.getBias(l,j) )
      discrDict = {
                    'nodes':   npCurrent.int_array(n),
                    'weights': npCurrent.fp_array(w),
                    'bias':    npCurrent.fp_array(b)
                  }
    discrDict['numberOfFusedDatasets'] = len(self.dataCurator.dataLocation)
    self._debug('Extracted discriminator to raw dictionary.')
    return discrDict

  def __dict_to_discr( self, discrDict, nnName=None, pruneLastLayer=True ):
    """
    Transform dictionaries of networks into discriminators.
    """
    if coreConf() is TuningToolCores.keras:
      from keras.models import model_from_json
      import json
      model = model_from_json( json.dumps(discrDict['model'], separators=(',', ':'))  )
      model.set_weights( discrDict['weights'] )
      return model
    elif coreConf() is TuningToolCores.FastNet:
      # TODO Implement a class to encapsulate this
      return discrDict

  def __concatenate_patterns(self, patterns):
    if type(patterns) not in (list,tuple):
      self._fatal('Input must be a tuple or list')
    pSize = [pat.shape[npCurrent.odim] for pat in patterns]
    target = npCurrent.fp_ones(npCurrent.shape(npat=1,nobs=np.sum(pSize)))
    # FIXME Could I use flag_ones?
    target[npCurrent.access(pidx=0,oidx=slice(pSize[0],None))] = -1.
    data = npCurrent.fix_fp_array( np.concatenate(patterns,axis=npCurrent.odim) )
    return data, target


  # FIXME This is not work when I call the undo preproc function.
  # The target is None for some reason...
  def __separate_patterns(self, data, target):
    patterns = list()
    classTargets = [1., -1.] # np.unique(target).tolist()
    for idx, classTarget in enumerate(classTargets):
      patterns.append( data[ npCurrent.access( pidx=':', oidx=np.where(target==classTarget)[0]) ] )
      self._debug('Separated pattern %d shape is %r', idx, patterns[idx].shape)
    return patterns
