
# The Official Ringer 2018 Tuning (v8)

The official scripts used to produce the monte carlo (v8) 2018 tuning.
It was used 2017 data samples (all year) to derive all neural networks.
These scripts are responsible to create the tuning data, launch to the
LCG grid, get the crossvalidation files, generate the tables and extract
the neural networks.


# Create data step

# tuning parameters

# cross validation

# extraction


