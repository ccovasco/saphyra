
# The Official Ringer 2017 Tuning (v6)

The official scripts used to produce the monte carlo (v6) 2017 tuning.
It was used Monte Carlo 2015 samples to derive all neural networks.
These scripts are responsible to create the tuning data, launch to the
LCG grid, get the crossvalidation files, generate the tables and extract
the neural networks.


# Create data step

# tuning parameters

# cross validation

# extraction


